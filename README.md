# @unisot/unisot-did-resolver

This module encapsulates the logic required to resolve UNISOT DID identities. The exported interface is compatible with the [DIF DID-Resolver](https://github.com/decentralized-identity/did-resolver) module.

_Please note that this module is not meant to be used directly, but rather through an integration with the DIF DID-Resolver module (as shown in the example below)._

## Usage examples

In combination with the [DIF DID-Resolver](https://github.com/decentralized-identity/did-resolver):

```javascript
import {Resolver} from "did-resolver";
import {getResolver} from "unisot-did-resolver";

const resolver = new Resolver(getResolver());
const didDocument = await resolver.resolve(did);
// didDocument now contains the corresponding Did Document in JSON form.
```
